﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace plex_renamer_thing
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = "C:\\Users";
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
               textBox3.Text = dialog.FileName + "\\";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked) {
                if (textBox2.Text != "" && textBox1.Text != "" && textBox3.Text != "")
                {
                    if (textBox2.Text.Length < 2)
                    {
                        textBox2.Text = "0" + textBox2.Text;
                    }
                    string[] filePaths = Directory.GetFiles(textBox3.Text);
                    int i = (int)numericUpDown1.Value;

                    foreach (var s in filePaths)
                    {

                        string seriesName = textBox1.Text.Replace(' ', '.')+".";
                        string season = textBox2.Text;
                        string oldFileName = s;

                        string path = Path.GetDirectoryName(s) + "\\";
                        string extention = Path.GetExtension(s);
                        string epnum = "";
                        if (i < 10)
                        {
                            epnum = "0" + i++.ToString();
                        }
                        else epnum = i++.ToString();
                        string newFileName = path + seriesName + "s" + season + $"e{epnum}";
                        if(textBox4.Text != "")
                        {
                            newFileName += "."+textBox4.Text.Replace(' ', '.');
                        }
                        newFileName.Replace(' ', '.');
                        newFileName += extention;
                        File.Move(oldFileName, newFileName);
                    }
                }

        }
            else if(!checkBox2.Checked && checkBox1.Checked) MessageBox.Show("Please enter the series name, the season, and the path to the files to continue." + Environment.NewLine + "Or dont and just leave. I really dont care." + Environment.NewLine + "bitch...");

            if(checkBox2.Checked )
            {
                if (textBox1.Text != "" && textBox3.Text != "")
                {
                    string[] filePaths = Directory.GetFiles(textBox3.Text);
                    int i = (int)numericUpDown1.Value;

                    foreach (var s in filePaths)
                    {

                        string seriesName = textBox1.Text.Replace(' ', '.')+".";

                        string oldFileName = s;

                        string path = Path.GetDirectoryName(s) + "\\";
                        string extention = Path.GetExtension(s);
                        string epnum = "";
                        if (i < 10)
                        {
                            epnum = "00" + i++.ToString();
                        }
                        else if (i < 10)
                        {
                            epnum = "0" + i++.ToString();
                        }
                        else epnum = i++.ToString();
                        string newFileName = path + seriesName + $"{epnum}" + extention;

                        File.Move(oldFileName, newFileName);
                    }
                }
            }
            else if (!checkBox1.Checked && checkBox2.Checked) MessageBox.Show("Please enter the series name and the path to the files to continue." + Environment.NewLine + "Or dont and just leave. I really dont care." + Environment.NewLine + "bitch...");

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //checkBox1.Checked = true;
            checkBox2.Checked = false;
            textBox2.Enabled = true;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            //checkBox2.Checked = true;
            checkBox1.Checked = false;
            textBox2.Enabled = false;

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
